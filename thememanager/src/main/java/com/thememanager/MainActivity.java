package com.thememanager;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    private RecyclerView recyclerView;

    private ArrayList<ThemeEntity> mThemeList = new ArrayList<>();

    private String[] mThemes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        // GridLayoutManager
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);

//        ThemeEntity themeEntity = new ThemeEntity();
//        themeEntity.mName = "ceshi 1";
//        themeEntity.mThumb = BitmapUtils.drawableToBitmap(this, R.drawable.thumbnail);
//        themeEntity.mUsing = true;
//        mThemeList.add(themeEntity);
//
//        themeEntity.mName = "ceshi 2";
//        themeEntity.mThumb = BitmapUtils.drawableToBitmap(this, R.drawable.thumbnail);
//        themeEntity.mUsing = false;
//        mThemeList.add(themeEntity);
//
//        themeEntity.mName = "ceshi 3";
//        themeEntity.mThumb = BitmapUtils.drawableToBitmap(this, R.drawable.thumbnail);
//        themeEntity.mUsing = false;
//        mThemeList.add(themeEntity);

//        ThemeListAdapter adapter = new ThemeListAdapter(this, mThemeList);
//        recyclerView.setAdapter(adapter);


        mThemes = getResources().getStringArray(R.array.theme_name);

        getInstalledTheme();

    }

    private void getInstalledTheme() {
        try {
            PackageManager pm = getPackageManager();
            PackageInfo packageInfo = pm.getPackageInfo(mThemes[0], 0);
            if (packageInfo != null) {
                ApplicationInfo appInfo = packageInfo.applicationInfo;

                String appName = appInfo.loadLabel(pm).toString();
                Drawable icon = appInfo.loadIcon(pm);
                Log.d("dingfeng", "appName:" + appName);
                Log.d("dingfeng", "packageName:" + packageInfo.packageName);

                Context themeContext = createPackageContext(mThemes[0], CONTEXT_IGNORE_SECURITY);
                Resources themeResources = themeContext.getResources();
                Drawable thumb = ReflectionUtil.drawable(themeResources, mThemes[0], "thumbnail");

                ThemeEntity themeEntity = new ThemeEntity();
                themeEntity.mName = appName;
                themeEntity.mPkgName = mThemes[0];
                themeEntity.mThumb = BitmapUtils.drawableToBitmap(thumb);
                themeEntity.mUsing = true;
                mThemeList.add(themeEntity);

                ThemeListAdapter adapter = new ThemeListAdapter(this, mThemeList);
                recyclerView.setAdapter(adapter);
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    class ThemeEntity {
        public String mName;
        public String mPkgName;
        public Bitmap mThumb;
        public boolean mUsing;
    }

    class ThemeListAdapter extends RecyclerView.Adapter<ViewHolder> {

        private Context mContext;
        private ArrayList<ThemeEntity> mThemeList = new ArrayList<>();

        public ThemeListAdapter(Context context, ArrayList<ThemeEntity> list) {
            mContext = context;
            mThemeList = list;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = View.inflate(mContext, R.layout.item_theme_list, null);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final ThemeEntity item = mThemeList.get(position);
            holder.thumb.setImageBitmap(item.mThumb);
            holder.txtName.setText(item.mName);
            if (item.mUsing) {
                holder.check.setVisibility(View.VISIBLE);
            } else {
                holder.check.setVisibility(View.GONE);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(mContext, ThemePreview.class);
                    intent.putExtra("theme_package", item.mPkgName);
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mThemeList.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        ImageView thumb;
        ImageView check;

        public ViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            thumb = (ImageView) itemView.findViewById(R.id.thumb);
            check = (ImageView) itemView.findViewById(R.id.check);
        }

    }

}
