package com.thememanager;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

/**
 * Created by dingfeng on 2016/7/21.
 */
public class ReflectionUtil {

    /**
     * 基于包名获取Context实例
     *
     * @param context
     * @param skinPkgName
     *            包名
     * @return
     * @throws NameNotFoundException
     */
    public static Context getSkinContext(Context context, String skinPkgName)
            throws PackageManager.NameNotFoundException {
        return context.createPackageContext(skinPkgName,
                Context.CONTEXT_IGNORE_SECURITY);
    }

    /**
     *
     * @param context
     * @param skinPkgName
     *            包名
     * @param name
     *            资源名称
     * @param defType
     *            资源类型，如color,drawable等
     * @return
     * @throws NameNotFoundException
     */
    public static int getIdentifier(Context context, String skinPkgName,
                                    String name, String defType) throws PackageManager.NameNotFoundException {
        return getSkinContext(context, skinPkgName).getResources()
                .getIdentifier(name, defType, skinPkgName);
    }

    /**
     * 基于getIdentifier对color的封装，便于获取颜色资源文件
     *
     * @param context
     * @param skinPkgName
     *            包名
     * @param name
     *            颜色名称
     * @return
     * @throws NameNotFoundException
     */
    public static int color(Context context, String skinPkgName, String name)
            throws PackageManager.NameNotFoundException {
        return getSkinContext(context, skinPkgName).getResources().getColor(
                getIdentifier(context, skinPkgName, name, "color"));
    }
    public static int color(Resources resources, String skinPkgname, String name) throws Resources.NotFoundException {
        return resources.getColor(resources.getIdentifier(name, "color", skinPkgname));
    }

    /**
     * 基于getIdentifier对dimension的封装，便于获取颜色资源文件
     *
     * @param context
     * @param skinPkgName
     *            包名
     * @param name
     *            dimension的名称
     * @return
     * @throws NameNotFoundException
     */
    public static float dimen(Context context, String skinPkgName, String name)
            throws PackageManager.NameNotFoundException {
        return getSkinContext(context, skinPkgName).getResources()
                .getDimension(getIdentifier(context, skinPkgName, name, "dimen"));
    }

    /**
     * 基于getIdentifier对drawable的封装，便于获取颜色资源文件
     *
     * @param context
     * @param skinPkgName
     *            包名
     * @param name
     *            drawable的名称
     * @return
     * @throws NameNotFoundException
     */
    public static Drawable drawable(Context context, String skinPkgName, String name) throws PackageManager.NameNotFoundException {
        return getSkinContext(context, skinPkgName).getResources().getDrawable(
                getIdentifier(context, skinPkgName, name, "drawable"));
    }

    /**
     * 通过资源的Resources对象来获取图片资源文件
     *
     * @param resources
     * @param skinPkgname
     * @param name
     * @return
     * @throws NameNotFoundException
     */
    public static Drawable drawable(Resources resources, String skinPkgname, String name) throws Resources.NotFoundException {
        return resources.getDrawable(resources.getIdentifier(name, "drawable", skinPkgname));
    }

    /**
     * 基于getIdentifier对string的封装，便于获取颜色资源文件
     *
     * @param context
     * @param skinPkgName
     *            包名
     * @param name
     *            string的名称
     * @return
     * @throws NameNotFoundException
     */
    public static String string(Context context, String skinPkgName, String name)
            throws PackageManager.NameNotFoundException {
        return getSkinContext(context, skinPkgName).getResources().getString(
                getIdentifier(context, skinPkgName, name, "string"));
    }
    /**
     * 基于getIdentifier对string的封装，便于获取asserts下的字体
     *
     * @param context
     * @param skinPkgName     包名
     * @param name            string的名称
     * @return
     * @throws NameNotFoundException
     */
    public static Typeface Font(Resources resources,String name)
            throws PackageManager.NameNotFoundException {
        Typeface customFont = null;
        try {
            customFont = Typeface.createFromAsset(resources.getAssets(), name);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return customFont;
    }
}
