package com.android.theme;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;

import com.android.launcher3.LauncherAppState;

/**
 * Created by dingfeng on 2016/7/22.
 */
public class ThemeManager {

    private static Context sContext;
    private String mCurrentTheme;
    private String mThemePackage;

    private static ThemeManager sInstance;

    public static synchronized ThemeManager getInstance() {
        if (sInstance == null) {
            sInstance = new ThemeManager();
            sContext = LauncherAppState.getInstance().getContext();
        }
        return sInstance;
    }

    public void setThemePackage(String pkg) {
        mThemePackage = pkg;
    }

    public Resources getThemeResource() {
        Resources themeResources = null;
        try {
            Context themeContext = sContext.createPackageContext(mThemePackage, Context.CONTEXT_IGNORE_SECURITY);
            themeResources = themeContext.getResources();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return themeResources;
    }


}
